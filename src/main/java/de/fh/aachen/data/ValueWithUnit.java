package de.fh.aachen.data;

import de.fh.aachen.timeseries.TimeseriesUnit;

import java.math.BigDecimal;

/**
 *
 */
public class ValueWithUnit {

    private BigDecimal value;
    private TimeseriesUnit unit;

    public ValueWithUnit(BigDecimal value, TimeseriesUnit unit) {
        this.value = value;
        this.unit = unit;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public TimeseriesUnit getUnit() {
        return unit;
    }

    public void setUnit(TimeseriesUnit unit) {
        this.unit = unit;
    }

    public ValueWithUnit toUnit(TimeseriesUnit unit){
        if(unit == null) throw new IllegalArgumentException("unit must not be null.");
        if(this.unit.isIntegral() != unit.isIntegral()) throw new IllegalStateException("cannot change unit from integral to none without knowledge of period.");
        final BigDecimal value;
        final int conversionExponent = this.unit.getExponent() - unit.getExponent();
        if(conversionExponent != 0){
            value = this.value.multiply(BigDecimal.ONE.movePointRight(conversionExponent)); //bei pow mit negativen Zahlen fliegt eine ArithmeticException
        }else{
            value = this.value;
        }

        return new ValueWithUnit(value, unit);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValueWithUnit)) return false;

        ValueWithUnit that = (ValueWithUnit) o;

        if (value != null ? value.compareTo(that.value)!=0 : that.value != null) return false;
        return unit == that.unit;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (unit != null ? unit.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ValueWithUnit{" +
                "value=" + value +
                ", unit=" + unit +
                '}';
    }

}
