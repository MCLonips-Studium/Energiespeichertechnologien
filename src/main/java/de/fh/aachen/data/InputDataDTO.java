package de.fh.aachen.data;

import de.fh.aachen.timeseries.Timeseries;

import java.math.BigDecimal;

/**
 *
 */
public class InputDataDTO {
    private Timeseries pvProduction_scaled;
    private Timeseries consumption;

    private ValueWithUnit batterysize;
    private BigDecimal usableCapacity;
    private BigDecimal chargeEfficiency;
    private BigDecimal dischargeEfficiency;
    private BigDecimal inverterEfficiency;

    public InputDataDTO() {
    }

    public Timeseries getPvProduction_scaled() {
        return pvProduction_scaled;
    }

    public InputDataDTO setPvProduction_scaled(Timeseries pvProduction_scaled) {
        this.pvProduction_scaled = pvProduction_scaled;
        return this;
    }

    public Timeseries getConsumption() {
        return consumption;
    }

    public InputDataDTO setConsumption(Timeseries consumption) {
        this.consumption = consumption;
        return this;
    }

    public ValueWithUnit getBatterysize() {
        return batterysize;
    }

    public InputDataDTO setBatterysize(ValueWithUnit batterysize) {
        this.batterysize = batterysize;
        return this;
    }

    public BigDecimal getUsableCapacity() {
        return usableCapacity;
    }

    public InputDataDTO setUsableCapacity(BigDecimal usableCapacity) {
        this.usableCapacity = usableCapacity;
        return this;
    }

    public BigDecimal getChargeEfficiency() {
        return chargeEfficiency;
    }

    public InputDataDTO setChargeEfficiency(BigDecimal chargeEfficiency) {
        this.chargeEfficiency = chargeEfficiency;
        return this;
    }

    public BigDecimal getDischargeEfficiency() {
        return dischargeEfficiency;
    }

    public InputDataDTO setDischargeEfficiency(BigDecimal dischargeEfficiency) {
        this.dischargeEfficiency = dischargeEfficiency;
        return this;
    }

    public BigDecimal getInverterEfficiency() {
        return inverterEfficiency;
    }

    public InputDataDTO setInverterEfficiency(BigDecimal inverterEfficiency) {
        this.inverterEfficiency = inverterEfficiency;
        return this;
    }
}
