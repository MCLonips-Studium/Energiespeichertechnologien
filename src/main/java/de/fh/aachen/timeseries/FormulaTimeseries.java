package de.fh.aachen.timeseries;

import de.fh.aachen.time.Interval;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 *
 */
public class FormulaTimeseries extends Timeseries {

    private final Function<Instant, BigDecimal> function;

    public FormulaTimeseries(Duration period, TimeseriesUnit unit,Function<Instant, BigDecimal> function) {
        super(period, unit);
        this.function = function;
    }

    @Override
    public BigDecimal getValue(Instant instant) {
        return function.apply(instant);
    }

    @Override
    public Interval getSpan() {
        return Interval.INFINITY;
    }
}
