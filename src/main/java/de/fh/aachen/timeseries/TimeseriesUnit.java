package de.fh.aachen.timeseries;

/**
 *
 */
public enum TimeseriesUnit {
    KILO_WATT_STUNDE(true, 3, "kWh"),
    WATT_STUNDE(true, 0, "Wh"),
    KILO_WATT(false, 3, "kW"),
    WATT(false, 0, "W"),
    NO_UNIT(false, 0, "[-]");

    private final boolean integral;
    private final int exponent;
    private final String symbol;

    TimeseriesUnit(boolean integral, int exponent, String symbol) {
        this.integral = integral;
        this.exponent = exponent;
        this.symbol = symbol;
    }

    public boolean isIntegral() {
        return integral;
    }

    public int getExponent() {
        return exponent;
    }

    public String getSymbol() {
        return symbol;
    }

    @Override
    public String toString() {
        return getSymbol();
    }

    public static TimeseriesUnit parse(String symbol){
        for (TimeseriesUnit timeseriesUnit : TimeseriesUnit.values()) {
            if(timeseriesUnit.getSymbol().equals(symbol)){
                return timeseriesUnit;
            }
        }
        return null;
    }
}
