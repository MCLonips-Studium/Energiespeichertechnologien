package de.fh.aachen.timeseries;

import de.fh.aachen.time.Interval;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 */
public class SourceTimeseries extends Timeseries {

    private final SortedMap<Instant, BigDecimal> values;

    public SourceTimeseries(Duration period, TimeseriesUnit unit) {
        super(period, unit);
        this.values = new TreeMap<>();
    }

    public SourceTimeseries(Duration period, TimeseriesUnit unit,SortedMap<Instant, BigDecimal> values) {
        super(period, unit);
        this.values = values;
    }

    @Override
    public BigDecimal getValue(Instant instant) {
        return values.get(instant);
    }

    @Override
    public Map<Instant, BigDecimal> getValues(Interval interval) {
        return values.subMap(interval.getStart(), interval.getEnd());
    }

    @Override
    public Interval getSpan() {
        return values.isEmpty()?null:new Interval(values.firstKey(), (Instant) period.addTo(values.lastKey()));
    }

    public void setValue(Instant instant, BigDecimal value){
        values.put(instant, value);
    }
}
