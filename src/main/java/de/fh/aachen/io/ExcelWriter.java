package de.fh.aachen.io;

import de.fh.aachen.simulation.StorageSimulation;
import de.fh.aachen.time.Interval;
import de.fh.aachen.timeseries.Timeseries;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;

/**
 *
 */
public class ExcelWriter {

    public static void writeFile(String outputPath, StorageSimulation data) throws IOException, InvalidFormatException {
        final XSSFWorkbook workbook;
        final XSSFSheet dataSheet, overviewSheet;

        final InputStream inputStream = ExcelWriter.class.getClassLoader().getResourceAsStream("Template.xlsx");
        ExcelWriter.class.getClassLoader().getResource("Template.xlsx");
        final File file = new File("src/main/resources/Template.xlsx");
        if(file.exists()){
            workbook = new XSSFWorkbook(file);
            overviewSheet = workbook.getSheetAt(0);
            dataSheet = workbook.getSheetAt(1);
        }else{
            workbook = new XSSFWorkbook();
            overviewSheet = workbook.createSheet("Überblick");
            dataSheet = workbook.createSheet("Daten");
        }
        writeOverview(overviewSheet, data);
        writeTimeseries(dataSheet, data);

        final FileOutputStream outputStream = new FileOutputStream(outputPath);
        workbook.write(outputStream);
        outputStream.close();
    }

    private static void writeOverview(XSSFSheet sheet, StorageSimulation data){
        writeValueToCell(sheet.getRow(1), 1, data.getAutarkiegrad());
        writeValueToCell(sheet.getRow(2), 1, data.getEigenverbrauchsquote());

//        final Timeseries pvTimeseries = data.getPvErzeugung();
//        final BigDecimal gesamtErzeugung = pvTimeseries.reduce(pvTimeseries.getSpan(), Timeseries.sumReducer());
//        writeValueToCell(sheet.getRow(4), 1, gesamtErzeugung);
//
//        final Timeseries einspeisungTimeseries = data.getEinspeisung();
//        final BigDecimal einspeisung = einspeisungTimeseries.reduce(einspeisungTimeseries.getSpan(), Timeseries.sumReducer());
//        writeValueToCell(sheet.getRow(5), 1, einspeisung);
//        writeValueToCell(sheet.getRow(6), 1, data.getEigenverbrauch());
//
//        final Timeseries verbrauchTimeseries = data.getVerbrauch();
//        final BigDecimal gesamtverbrauch = verbrauchTimeseries.reduce(verbrauchTimeseries.getSpan(), Timeseries.sumReducer());
//        writeValueToCell(sheet.getRow(7), 1, gesamtverbrauch);
//
//        final Timeseries netzbezugTimeseries = data.getNetzbezug();
//        final BigDecimal netzbezug = netzbezugTimeseries.reduce(netzbezugTimeseries.getSpan(), Timeseries.sumReducer());
//        writeValueToCell(sheet.getRow(8), 1, netzbezug);
    }

    private static void writeTimeseries(XSSFSheet sheet, StorageSimulation data) {
        final Interval span = data.getSpan();
        final Duration period = data.getPeriod();

        Instant curInstant = span.getStart();
        int rowIndex = 1;
        while (curInstant.isBefore(span.getEnd())){
            XSSFRow row = sheet.getRow(rowIndex);
            if(row == null) row = sheet.createRow(rowIndex);

            writeValueToCell(row,0, curInstant);
            writeValueToCell(row,1, data.getPvErzeugung().getValue(curInstant));
            writeValueToCell(row,2, data.getVerbrauch().getValue(curInstant));
            writeValueToCell(row,3, data.getSpeicherverlauf().getValue(curInstant));
            writeValueToCell(row,4, data.getEinspeisung().getValue(curInstant));
            writeValueToCell(row,5, data.getNetzbezug().getValue(curInstant));

            rowIndex++;
            curInstant = (Instant) period.addTo(curInstant);
        }
    }

    private static void writeValueToCell(XSSFRow row, int column, Object value){
        if(value == null) return;
        XSSFCell cell = row.getCell(column);
        if(cell == null) cell = row.createCell(column);

        cell.setCellValue(value.toString());
    }

    private static void writeValueToCell(XSSFRow row, int column, BigDecimal value){
        if(value == null) return;
        XSSFCell cell = row.getCell(column);
        if(cell == null) cell = row.createCell(column);

        cell.setCellValue(value.doubleValue());
    }

    private static void writeValueToCell(XSSFRow row, int column, Instant value){
        if(value == null) return;
        XSSFCell cell = row.getCell(column);
        if(cell == null) cell = row.createCell(column);

        cell.setCellValue(Date.from(value));
    }
}
