package de.fh.aachen.io;

import de.fh.aachen.data.InputDataDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.timeseries.SourceTimeseries;
import de.fh.aachen.timeseries.Timeseries;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * class to read all necessary data from excel
 */
public class ExcelReader {

    public static InputDataDTO readFile(String file) throws IOException {
        final InputDataDTO dto = new InputDataDTO();

        final Workbook workbook = new XSSFWorkbook(file);
        readBatteryData(workbook, dto);
        readPVProduction(workbook, dto);
        readConsumption(workbook, dto);

        return dto;
    }

    //package local for testing issues
    static void readBatteryData(Workbook workbook, InputDataDTO dto) {
        final Sheet sheet = workbook.getSheetAt(0);
        {
            final Row row = sheet.getRow(7);
            final Cell inverterEfficiencyCell = row.getCell(1);
            final double value = inverterEfficiencyCell.getNumericCellValue();

            dto.setInverterEfficiency(BigDecimal.valueOf(value).movePointLeft(2)); //Wert in %
        }
        {
            final Row row = sheet.getRow(11);
            final Cell batterysizeCell = row.getCell(1);
            final double value = batterysizeCell.getNumericCellValue();
            final String formatString = batterysizeCell.getCellStyle().getDataFormatString(); //##,##0.0\ "  kWh"
            final TimeseriesUnit unit = TimeseriesUnit.parse(formatString); //TODO extract unit symbol from format string

            dto.setBatterysize(new ValueWithUnit(new BigDecimal(value), TimeseriesUnit.KILO_WATT_STUNDE)); //TODO use parsed unit
        }
        {
            final Row row = sheet.getRow(12);
            final Cell usableCapacityCell = row.getCell(1);
            final double usableCapacity = usableCapacityCell.getNumericCellValue(); // 90.0
            dto.setUsableCapacity(BigDecimal.valueOf(usableCapacity).movePointLeft(2)); //Wert in %
        }
        {
            final Row row = sheet.getRow(13);
            final Cell chargeEfficiencyCell = row.getCell(1);
            final double chargeEfficiency = chargeEfficiencyCell.getNumericCellValue(); // 98.0
            dto.setChargeEfficiency(BigDecimal.valueOf(chargeEfficiency).movePointLeft(2)); //Wert in %
        }
        {
            final Row row = sheet.getRow(14);
            final Cell dischargeEfficiencyCell = row.getCell(1);
            final double dischargeEfficiency = dischargeEfficiencyCell.getNumericCellValue(); // 97.0
            dto.setDischargeEfficiency(BigDecimal.valueOf(dischargeEfficiency).movePointLeft(2)); //Wert in %
        }
    }

    static void readPVProduction(Workbook workbook, InputDataDTO dto){
        final Sheet productionSheet = workbook.getSheetAt(1);
        final Timeseries pvProduction = readTimeseries(productionSheet, 2, 0, 2);
        dto.setPvProduction_scaled(pvProduction);
    }

    static void readConsumption(Workbook workbook, InputDataDTO dto){
        final Sheet consumptionSheet = workbook.getSheetAt(2);
        final Timeseries consumption = readTimeseries(consumptionSheet, 2, 0, 1);
        dto.setConsumption(consumption);
    }

    static Timeseries readTimeseries(Sheet sheet, int startRow, int columnTimestamps, int columnValues){
        final SortedMap<Instant, BigDecimal> values = new TreeMap<>();
        final TimeZone timeZone = TimeZone.getTimeZone("GMT+1");

        final Iterator<Row> rowIterator = sheet.rowIterator();
        Row row;
        while(rowIterator.hasNext()){
            row = rowIterator.next();
            if(row.getRowNum() < startRow){
                continue;
            }

            final Cell timestampCell = row.getCell(columnTimestamps);
            final Instant instant;
            if(timestampCell!=null && timestampCell.getCellTypeEnum() == CellType.NUMERIC){
//                final Date dateCellValue = timestampCell.getDateCellValue();
//                instant = dateCellValue.toInstant();
                final double numericCellValue = timestampCell.getNumericCellValue();
                instant = DateUtil.getJavaDate(numericCellValue,false, timeZone).toInstant();
            }else{
                continue;
            }

            final Cell valueCell = row.getCell(columnValues);
            final BigDecimal value;
            if(valueCell!=null && valueCell.getCellTypeEnum() == CellType.NUMERIC){
                final double numericCellValue = valueCell.getNumericCellValue();
                value = BigDecimal.valueOf(numericCellValue);
            }else{
                value = null;
            }

            values.put(instant, value);
        }

        return new SourceTimeseries(Duration.of(5, ChronoUnit.MINUTES), TimeseriesUnit.WATT, values);
    }
}
