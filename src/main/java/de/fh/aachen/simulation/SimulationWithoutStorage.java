package de.fh.aachen.simulation;

import de.fh.aachen.data.InputDataDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.time.Interval;
import de.fh.aachen.timeseries.ConstantValueTimeseries;
import de.fh.aachen.timeseries.SourceTimeseries;
import de.fh.aachen.timeseries.Timeseries;
import de.fh.aachen.timeseries.TimeseriesUnit;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Duration;
import java.time.Instant;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 *
 */
public class SimulationWithoutStorage implements Simulation {

    /**
     * Einheit, in der gerechnet wird
     */
    private static final TimeseriesUnit calculationUnit = TimeseriesUnit.WATT_STUNDE;

    /**
     * Zeitreihe, die die erzeugte Energie der PV-Anlage beschreibt
     */
    private Timeseries pvErzeugung;

    /**
     * Zeitreihe, die den benoetigten/verbrauchten Strom beschreibt
     */
    private Timeseries verbrauch;
    /**
     * Zeitreihe, die die eingespeiste Energie beschreibt
     */
    private Timeseries einspeisung;

    /**
     * Zeitreihe, die den Strombezug aus dem Netz beschreibt
     */
    private Timeseries netzbezug;

    private Interval span;

    private Duration period;

    public SimulationWithoutStorage(InputDataDTO inputData){
        final TimeseriesUnit unit = inputData.getBatterysize().getUnit();
        this.period = inputData.getPvProduction_scaled().getPeriod();
        pvErzeugung = inputData.getPvProduction_scaled()
                .toUnit(calculationUnit)
                .multiply(new ConstantValueTimeseries(period,TimeseriesUnit.NO_UNIT, inputData.getInverterEfficiency())); //Wirkungsgrad des Wechselrichters einbeziehen
        verbrauch = inputData.getConsumption().toUnit(calculationUnit);
        this.span = inputData.getConsumption().getSpan(); //sourcetimeseries; alle anderen sind formeln und daher kann span nicht angegeben werden
        calculateSpeicherverlaufAndEinspeisung(inputData);
    }

    /**
     * Diese Funktion berechnet die Zeitreihen für den Speicherverlauf und die Einspeisung
     * @param inputData
     */
    private void calculateSpeicherverlaufAndEinspeisung(InputDataDTO inputData){
        SortedMap<Instant, BigDecimal> storage = new TreeMap<>();
        SortedMap<Instant, BigDecimal> einspeisung = new TreeMap<>();
        SortedMap<Instant, BigDecimal> netzbezug = new TreeMap<>();

        final Duration period = pvErzeugung.getPeriod();
        final ValueWithUnit batterysize = inputData.getBatterysize().toUnit(calculationUnit);
        final BigDecimal usableBatterySize = batterysize.getValue().multiply(inputData.getUsableCapacity());
        Instant curInstant = span.getStart();
        Instant instantBefore = (Instant) period.subtractFrom(curInstant);
        while(curInstant.isBefore(span.getEnd())){
            final BigDecimal pv = pvErzeugung.getValue(curInstant);

            if(pv == null){ //Es sind Lücken in der Zeitreihe!!! (29.02.2012)
                instantBefore = curInstant;
                curInstant = (Instant) period.addTo(curInstant);
                continue;
            }
            final BigDecimal consumption = verbrauch.getValue(curInstant);
            final BigDecimal delta = pv.subtract(consumption);

            final BigDecimal speicherstand = storage.getOrDefault(instantBefore, BigDecimal.ZERO);

            final BigDecimal einspeisungValue;
            final BigDecimal netzbezugValue;

            if(delta.compareTo(BigDecimal.ZERO) < 0){
                //momentane Erzeugung reicht nicht aus -> Strom muss aus dem Netz entnommen werden
                //delta ist negativ
                netzbezugValue = delta.negate().subtract(speicherstand.multiply(inputData.getDischargeEfficiency()));
                einspeisungValue = BigDecimal.ZERO;
            }else{
                //momentaner Verbrauch wird durch PV-Anlage gedeckt. Der Ueberschuss wird eingespeist
                //delta ist positiv
                einspeisungValue = delta;
                netzbezugValue = BigDecimal.ZERO;
            }

            einspeisung.put(curInstant, einspeisungValue);
            netzbezug.put(curInstant, netzbezugValue);

            instantBefore = curInstant;
            curInstant = (Instant) period.addTo(curInstant);
        }

        this.einspeisung = new SourceTimeseries(period, calculationUnit, einspeisung);
        this.netzbezug = new SourceTimeseries(period, calculationUnit, netzbezug);
    }

    @Override
    public BigDecimal getAutarkiegrad(){
        //Autarkiegrad [%] = eigenverbrauchter Solarstrom / Gesamtstromverbrauch

        final BigDecimal eigenverbrauch = getEigenverbrauch();
        final BigDecimal verbrauch = this.verbrauch.reduce(span, Timeseries.sumReducer());

        return eigenverbrauch.divide(verbrauch, MathContext.DECIMAL128);
    }

    @Override
    public BigDecimal getEigenverbrauchsquote(){
        //Eigenverbrauchquote [%] = eigenverbrauchter Solarstrom / gesamtproduzierter Solarstrom

        final BigDecimal eigenverbrauch = getEigenverbrauch();
        final BigDecimal erzeugung = this.pvErzeugung
                .reduce(span, Timeseries.sumReducer());

        return eigenverbrauch.divide(erzeugung, MathContext.DECIMAL128);
    }

    private BigDecimal getEigenverbrauch() {
        return pvErzeugung
                .subtract(einspeisung)
                .reduce(span, Timeseries.sumReducer());
    }

    public Timeseries getPvErzeugung() {
        return pvErzeugung;
    }

    public Timeseries getVerbrauch() {
        return verbrauch;
    }

    public Timeseries getEinspeisung() {
        return einspeisung;
    }

    public Timeseries getNetzbezug() {
        return netzbezug;
    }

    public Interval getSpan() {
        return span;
    }

    public Duration getPeriod() {
        return period;
    }
}
