package de.fh.aachen.simulation;

import java.math.BigDecimal;

/**
 *
 */
public interface Simulation {
    BigDecimal getAutarkiegrad();

    BigDecimal getEigenverbrauchsquote();
}
