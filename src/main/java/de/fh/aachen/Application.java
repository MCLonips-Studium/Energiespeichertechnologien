package de.fh.aachen;

import de.fh.aachen.data.InputDataDTO;
import de.fh.aachen.io.ExcelReader;
import de.fh.aachen.io.ExcelWriter;
import de.fh.aachen.simulation.Simulation;
import de.fh.aachen.simulation.SimulationWithoutStorage;
import de.fh.aachen.simulation.StorageSimulation;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.File;
import java.io.IOException;

/**
 *
 */
public class Application {
    /**
     * first arg should be the filepath of inputfile
     * @param args
     */
    public static void main(String[] args) throws IOException, InvalidFormatException {
        if(args.length == 0){
            System.out.println("filepath is required as program argument.");
            return;
        }

        final String file = args[0];
        System.out.println("Reading file " + (new File(args[0])).getAbsolutePath());
        final InputDataDTO inputData = ExcelReader.readFile(file);
        System.out.println("Calculating...");
        final StorageSimulation simulation = new StorageSimulation(inputData);
//        final Simulation simulation = new SimulationWithoutStorage(inputData);

//        System.out.println("Autarkiegrad: " + simulation.getAutarkiegrad());
//        System.out.println("Eigenverbrauchsquote: " + simulation.getEigenverbrauchsquote());

        System.out.println("Writing results to file...");
        ExcelWriter.writeFile("result.xlsx", simulation);
        System.out.println("Finished");
    }
}