package de.fh.aachen.time;

import java.time.Instant;

/**
 *
 */
public class Interval {

    public static final Interval INFINITY = new Interval(Instant.MIN, Instant.MAX);

    private final Instant start;
    private final Instant end;

    public Interval(Instant start, Instant end) {
        this.start = start;
        this.end = end;
    }

    public Instant getStart() {
        return start;
    }

    public Instant getEnd() {
        return end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Interval)) return false;

        Interval interval = (Interval) o;

        if (start != null ? !start.equals(interval.start) : interval.start != null) return false;
        return end != null ? end.equals(interval.end) : interval.end == null;
    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "[" + start + " ; " + end +']';
    }
}
