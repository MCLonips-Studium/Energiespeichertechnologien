package de.fh.aachen.data;

import de.fh.aachen.timeseries.Timeseries;

/**
 *
 */
public class ExpectedSimulationResult {
    Timeseries storage;
    Timeseries einspeisung;
    Timeseries netzbezug;

    public ExpectedSimulationResult(Timeseries storage, Timeseries einspeisung, Timeseries netzbezug) {
        this.storage = storage;
        this.einspeisung = einspeisung;
        this.netzbezug = netzbezug;
    }

    public Timeseries getStorage() {
        return storage;
    }

    public Timeseries getEinspeisung() {
        return einspeisung;
    }

    public Timeseries getNetzbezug() {
        return netzbezug;
    }
}
