package de.fh.aachen;

import de.fh.aachen.data.ExpectedSimulationResult;
import de.fh.aachen.data.InputDataDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.simulation.StorageSimulation;
import de.fh.aachen.time.Interval;
import de.fh.aachen.timeseries.ConstantValueTimeseries;
import de.fh.aachen.timeseries.SourceTimeseries;
import de.fh.aachen.timeseries.Timeseries;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ObjectArrayArguments;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Stream;

import static de.fh.aachen.timeseries.TimeseriesUnit.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;

/**
 *
 */
public class StorageSimulationTest {
    private static Duration PT15M = Duration.ofMinutes(15);
    private static Instant _01_01_17_00_00 = Instant.parse("2017-01-01T00:00:00Z");
    private static Instant _01_01_17_00_15 = Instant.parse("2017-01-01T00:15:00Z");
    private static Instant _01_01_17_00_30 = Instant.parse("2017-01-01T00:30:00Z");
    private static Instant _01_01_17_00_45 = Instant.parse("2017-01-01T00:45:00Z");
    private static Instant _01_01_17_01_00 = Instant.parse("2017-01-01T01:00:00Z");
    private static Instant _01_01_17_01_15 = Instant.parse("2017-01-01T01:15:00Z");
    private static BigDecimal epsilon = new BigDecimal("0.0000000001");

    @DisplayName("Berechne Zeoitreihen für Speicher und Einspeisung")
    @ParameterizedTest(name="{0}")
    @MethodSource(names = "createCalculationTestCases")
    public void calculate(String name, InputDataDTO input, ExpectedSimulationResult expectedResult){
        final StorageSimulation storageSimulation = new StorageSimulation(input);
        final Timeseries storage = storageSimulation.getSpeicherverlauf();
        final Timeseries einspeisung = storageSimulation.getEinspeisung();
        final Timeseries netzbezug = storageSimulation.getNetzbezug();

        final TimeseriesUnit unit = WATT_STUNDE;
        assertThat("wrong storage unit", storage.getUnit(), is(unit));
        assertThat("wrong einspeisung unit", einspeisung.getUnit(), is(unit));
        assertThat("wrong netzbezug unit", netzbezug.getUnit(), is(unit));

        final Interval interval = input.getPvProduction_scaled().getSpan();
        final Duration period = input.getPvProduction_scaled().getPeriod();
        Instant curInstant = interval.getStart();
        while(curInstant.isBefore(interval.getEnd())){
            assertThat("einspeisung value for " + curInstant + " does not match",einspeisung.getValue(curInstant), closeTo(expectedResult.getEinspeisung().getValue(curInstant), epsilon));
            assertThat("netzbezug value for " + curInstant + " does not match",netzbezug.getValue(curInstant), closeTo(expectedResult.getNetzbezug().getValue(curInstant), epsilon));
            assertThat("storage value for " + curInstant + " does not match",storage.getValue(curInstant), closeTo(expectedResult.getStorage().getValue(curInstant), epsilon));

            curInstant = (Instant) period.addTo(curInstant);
        }
    }

    public static Stream<Arguments> createCalculationTestCases(){
        List<Arguments> testCases = new ArrayList<>();
        testCases.add(createCalculationTestCase_speicherLaeuftUeber());
        testCases.add(createCalculationTestCase_speicherLeer());
        testCases.add(createCalculationTestCase_StrombezugVomSpeicher());

        return testCases.stream();
    }

    /**
     * Zunächst wird der Speicher Stück für Stück aufgeladen, bis dann nicht der komplette PV-Strom in den Speicher
     * passt und der Rest ins Netz eingespeist werden muss
     * @return
     */
    public static Arguments createCalculationTestCase_speicherLaeuftUeber(){
        final SortedMap<Instant, BigDecimal> pvMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> consumptionMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> storageMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> einspeisungMap = new TreeMap();

        Instant instant = _01_01_17_00_00;
        pvMap.put(instant, new BigDecimal("200")); //200kW * 0,97 = 194kW verbleiben
        consumptionMap.put(instant, new BigDecimal("100"));
        storageMap.put(instant, new BigDecimal("22.325")); // 94kW * 0,95 = 89,3kW in PT15M --> 22,325kWh
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_00_15;
        pvMap.put(instant, new BigDecimal("500")); //485kW
        consumptionMap.put(instant, new BigDecimal("100"));
        storageMap.put(instant, new BigDecimal("113.7625")); //385kW * 0,95 = 365,75kW in  --> 91,4375kWh
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_00_30;
        pvMap.put(instant, new BigDecimal("600")); //582
        consumptionMap.put(instant, new BigDecimal("0"));
        storageMap.put(instant, new BigDecimal("251.9875")); //582 * 0,95 /4 = 138,225
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_00_45;
        pvMap.put(instant, new BigDecimal("1000")); //970
        consumptionMap.put(instant, new BigDecimal("200"));
        storageMap.put(instant, new BigDecimal("434.8625")); //770 * 0,95 / 4 = 182,875
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_01_00;
        pvMap.put(instant, new BigDecimal("2000")); //1940
        consumptionMap.put(instant, new BigDecimal("400"));
        storageMap.put(instant, new BigDecimal("800.6125")); //1540*0,95/4=365,75
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_01_15;
        pvMap.put(instant, new BigDecimal("1000")); //970
        consumptionMap.put(instant, new BigDecimal("100"));
        storageMap.put(instant, new BigDecimal("900")); //Speicher kann nur bis 900 kWh aufgeladen werden -> 99,3875kWh kommen noch in den Speicher. Dazu werden ca. 104,618421kWh benötigt
        einspeisungMap.put(instant, new BigDecimal("112.8815789473")); //Ergebnis ist nur auf 10 Stellen genau

        final InputDataDTO input = new InputDataDTO()
                .setBatterysize(new ValueWithUnit(new BigDecimal("1"), KILO_WATT_STUNDE))
                .setUsableCapacity(new BigDecimal("0.9"))
                .setChargeEfficiency(new BigDecimal("0.95"))
                .setDischargeEfficiency(new BigDecimal("0.96"))
                .setInverterEfficiency(new BigDecimal("0.97"))
                .setPvProduction_scaled(new SourceTimeseries(PT15M, WATT, pvMap))
                .setConsumption(new SourceTimeseries(PT15M, WATT, consumptionMap));

        final Timeseries storage = new SourceTimeseries(PT15M, WATT_STUNDE, storageMap);
        final Timeseries einspeisung = new SourceTimeseries(PT15M, WATT_STUNDE, einspeisungMap);
        final Timeseries netzbezug = new ConstantValueTimeseries(PT15M, WATT_STUNDE, BigDecimal.ZERO);

        final ExpectedSimulationResult expectedResult = new ExpectedSimulationResult(storage, einspeisung, netzbezug);
        return ObjectArrayArguments.create("Strom wird eingespeist, wenn der Speicher voll ist", input, expectedResult);
    }

    /**
     * In dem Speicher ist nur ein geringer Teil und der Rest muss aus dem Netz entnommen werden.
     * @return
     */
    public static Arguments createCalculationTestCase_speicherLeer(){
        final SortedMap<Instant, BigDecimal> pvMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> consumptionMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> storageMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> einspeisungMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> netzbezugMap = new TreeMap();

        Instant instant = _01_01_17_00_00;
        pvMap.put(instant, new BigDecimal("200")); //194
        consumptionMap.put(instant, new BigDecimal("100"));
        storageMap.put(instant, new BigDecimal("22.325")); //94*0,95/4=22,325
        einspeisungMap.put(instant, BigDecimal.ZERO);
        netzbezugMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_00_15;
        pvMap.put(instant, new BigDecimal("200"));//194
        consumptionMap.put(instant, new BigDecimal("400"));
        storageMap.put(instant, BigDecimal.ZERO); //22,325*0,96=21,432kWh kommen aus dem Speicher
        einspeisungMap.put(instant, BigDecimal.ZERO);
        netzbezugMap.put(instant, new BigDecimal("30.068"));

        instant = _01_01_17_00_30;
        pvMap.put(instant, new BigDecimal("200")); //194
        consumptionMap.put(instant, new BigDecimal("400"));
        storageMap.put(instant, BigDecimal.ZERO);
        einspeisungMap.put(instant, BigDecimal.ZERO);
        netzbezugMap.put(instant, new BigDecimal("51.5"));

        final InputDataDTO input = new InputDataDTO()
                .setBatterysize(new ValueWithUnit(new BigDecimal("1"), KILO_WATT_STUNDE))
                .setUsableCapacity(new BigDecimal("0.9"))
                .setChargeEfficiency(new BigDecimal("0.95"))
                .setDischargeEfficiency(new BigDecimal("0.96"))
                .setInverterEfficiency(new BigDecimal("0.97"))
                .setPvProduction_scaled(new SourceTimeseries(PT15M, WATT, pvMap))
                .setConsumption(new SourceTimeseries(PT15M, WATT, consumptionMap));

        final Timeseries storage = new SourceTimeseries(PT15M, WATT_STUNDE, storageMap);
        final Timeseries einspeisung = new SourceTimeseries(PT15M, WATT_STUNDE, einspeisungMap);
        final Timeseries netzbezug = new SourceTimeseries(PT15M, WATT_STUNDE, netzbezugMap);

        final ExpectedSimulationResult expectedResult = new ExpectedSimulationResult(storage, einspeisung, netzbezug);
        return ObjectArrayArguments.create("Strom wird aus Netz entnommen, da Speicher leer", input, expectedResult);
    }

    /**
     * In dem Speicher ist nur ein geringer Teil und der Rest muss aus dem Netz entnommen werden.
     * @return
     */
    public static Arguments createCalculationTestCase_StrombezugVomSpeicher(){
        final SortedMap<Instant, BigDecimal> pvMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> consumptionMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> storageMap = new TreeMap();
        final SortedMap<Instant, BigDecimal> einspeisungMap = new TreeMap();

        Instant instant = _01_01_17_00_00;
        pvMap.put(instant, new BigDecimal("1000"));//970
        consumptionMap.put(instant, new BigDecimal("200"));
        storageMap.put(instant, new BigDecimal("182.875")); //770*0,95/4=182,875
        einspeisungMap.put(instant, BigDecimal.ZERO);

        instant = _01_01_17_00_15;
        pvMap.put(instant, new BigDecimal("200"));//194
        consumptionMap.put(instant, new BigDecimal("400"));
        storageMap.put(instant, new BigDecimal("129.2291666666")); //206/0,96/4=53,64583kWh werden aus dem Speicher entnommen
        einspeisungMap.put(instant, BigDecimal.ZERO);

        final InputDataDTO input = new InputDataDTO()
                .setBatterysize(new ValueWithUnit(new BigDecimal("1"), KILO_WATT_STUNDE))
                .setUsableCapacity(new BigDecimal("0.9"))
                .setChargeEfficiency(new BigDecimal("0.95"))
                .setDischargeEfficiency(new BigDecimal("0.96"))
                .setInverterEfficiency(new BigDecimal("0.97"))
                .setPvProduction_scaled(new SourceTimeseries(PT15M, WATT, pvMap))
                .setConsumption(new SourceTimeseries(PT15M, WATT, consumptionMap));

        final Timeseries storage = new SourceTimeseries(PT15M, WATT_STUNDE, storageMap);
        final Timeseries einspeisung = new SourceTimeseries(PT15M, WATT_STUNDE, einspeisungMap);
        final Timeseries netzbezug = new ConstantValueTimeseries(PT15M, WATT_STUNDE, BigDecimal.ZERO);

        final ExpectedSimulationResult expectedResult = new ExpectedSimulationResult(storage, einspeisung, netzbezug);
        return ObjectArrayArguments.create("Strom wird aus dem Speicher entnommen", input, expectedResult);
    }
}
