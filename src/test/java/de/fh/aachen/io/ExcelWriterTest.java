package de.fh.aachen.io;

import de.fh.aachen.simulation.StorageSimulation;
import de.fh.aachen.time.Interval;
import de.fh.aachen.timeseries.ConstantValueTimeseries;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 */
public class ExcelWriterTest {
    private static final Instant _01_01_17_00_00 = Instant.parse("2017-01-01T00:00:00Z");
    private static final Instant _01_02_17_00_00 = Instant.parse("2017-02-01T00:00:00Z");
    private static final Duration PT15M = Duration.ofMinutes(15);

    @Test
    public void writeToFile() throws IOException, InvalidFormatException {
        //create simulation data
        final TimeseriesUnit unit = TimeseriesUnit.KILO_WATT_STUNDE;
        final StorageSimulation simulation = mock(StorageSimulation.class);
        when(simulation.getSpan()).thenReturn(new Interval(_01_01_17_00_00, _01_02_17_00_00));
        when(simulation.getPeriod()).thenReturn(PT15M);
        when(simulation.getPvErzeugung()).thenReturn(new ConstantValueTimeseries(PT15M, unit, BigDecimal.ONE));
        when(simulation.getVerbrauch()).thenReturn(new ConstantValueTimeseries(PT15M, unit, new BigDecimal("2")));
        when(simulation.getSpeicherverlauf()).thenReturn(new ConstantValueTimeseries(PT15M, unit, new BigDecimal("3")));
        when(simulation.getEinspeisung()).thenReturn(new ConstantValueTimeseries(PT15M, unit, new BigDecimal("4")));
        when(simulation.getNetzbezug()).thenReturn(new ConstantValueTimeseries(PT15M, unit, new BigDecimal("5")));

//        try {
            ExcelWriter.writeFile("src/test/resources/Ausgangsdaten.xlsx", simulation);
//        }catch (Throwable t){
//            fail(t.getMessage());
//            t.printStackTrace();
//        }
    }
}
