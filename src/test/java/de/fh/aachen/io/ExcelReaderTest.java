package de.fh.aachen.io;

import de.fh.aachen.data.InputDataDTO;
import de.fh.aachen.data.ValueWithUnit;
import de.fh.aachen.time.Interval;
import de.fh.aachen.timeseries.Timeseries;
import de.fh.aachen.timeseries.TimeseriesUnit;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.OrderingComparison.comparesEqualTo;
import static org.junit.jupiter.api.Assertions.fail;

/**
 *
 */
public class ExcelReaderTest {

    private static final String filepath = "src/test/resources/Eingangsdaten_Programieraufgabe_EST_SS2017.xlsx";
    private static final Instant _01_01_2012 = Instant.parse("2011-12-31T23:00:00Z");
    private static final Instant _31_12_2012 = Instant.parse("2012-12-31T23:00:00Z");

    static Workbook workbook;

    @BeforeAll
    public static void setUp(){
        try {
            workbook = new XSSFWorkbook(filepath);
        } catch (IOException e) {
            fail("unable to run this test. " + e.getMessage());
        }
    }

    @Test
    public void readBatteryData(){
        final InputDataDTO dto = new InputDataDTO();
        ExcelReader.readBatteryData(workbook, dto);

        assertThat("interverter efficiency", dto.getInverterEfficiency(), comparesEqualTo(new BigDecimal("0.97")));
        assertThat(dto.getBatterysize(), is(new ValueWithUnit(BigDecimal.valueOf(2), TimeseriesUnit.KILO_WATT_STUNDE)));
        assertThat("usable capacity", dto.getUsableCapacity(), comparesEqualTo(new BigDecimal("0.9")));
        assertThat("charge efficiency", dto.getChargeEfficiency(), comparesEqualTo(new BigDecimal("0.98")));
        assertThat("discharge efficiency", dto.getDischargeEfficiency(), comparesEqualTo(new BigDecimal("0.97")));
    }

    @Test
    public void readPVProduction(){
        final Interval valueInterval = new Interval(_01_01_2012, _31_12_2012);

        final InputDataDTO dto = new InputDataDTO();
        ExcelReader.readPVProduction(workbook, dto);

        final Timeseries pvProduction = dto.getPvProduction_scaled();
        final Interval span = pvProduction.getSpan();
        assertThat(span, is(valueInterval));
        final Map<Instant, BigDecimal> values = pvProduction.getValues(valueInterval);
        assertThat("number of values", values.size(), is(105_120)); //in der Datei ist 2012 kein Schaltjahr
    }

    @Test
    public void readConsumption(){
        final Interval valueInterval = new Interval(_01_01_2012, _31_12_2012);

        final InputDataDTO dto = new InputDataDTO();
        ExcelReader.readConsumption(workbook, dto);

        final Timeseries consumption = dto.getConsumption();
        final Interval span = consumption.getSpan();
        assertThat(span, is(valueInterval));
        final Map<Instant, BigDecimal> values = consumption.getValues(valueInterval);
        assertThat("number of values", values.size(), is(105_120)); //in der Datei ist 2012 kein Schaltjahr
    }
}
